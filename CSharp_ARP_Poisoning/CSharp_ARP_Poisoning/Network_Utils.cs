﻿using SharpPcap;
using SharpPcap.LibPcap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PacketDotNet;
using System.Net.NetworkInformation;

namespace CSharp_ARP_Poisoning
{
    internal class Network_Utils // Deprecated
    {
        public static void ARP_Request(System.Net.IPAddress ipAddress, LibPcapLiveDevice device)
        {
            if (ipAddress == null)
                throw new Exception("ARP IP address Cannot be null");

            if (device == null)
                throw new Exception("device Cannot be null");

            for (int i = 0; i < device.Addresses.Count; i++)
            {
                var address = device.Addresses[i];
                Console.WriteLine(address);
            }
            var ethernetPacket = new EthernetPacket(device.Addresses[2].Addr.hardwareAddress, PhysicalAddress.Parse("FF-FF-FF-FF-FF-FF"), EthernetType.Arp);


            var arpPacket = new ArpPacket(ArpOperation.Request, PhysicalAddress.Parse("00-00-00-00-00-00"), ipAddress, device.Addresses[2].Addr.hardwareAddress, device.Addresses[1].Addr.ipAddress);
            ethernetPacket.PayloadPacket = arpPacket;

            device.Open();
            device.SendPacket(ethernetPacket);
            device.Close();
        }

        /// <summary>
        /// Prints the time and length of each received packet
        /// </summary>
        private static void device_OnPacketArrival(object sender, PacketCapture e)
        {
            var time = e.Header.Timeval.Date;
            var len = e.Data.Length;


            var rawPacket = e.GetPacket();

            EthernetPacket packet = (EthernetPacket)Packet.ParsePacket(e.GetPacket().LinkLayerType, rawPacket.Data);

            Console.WriteLine(packet);

            Console.WriteLine("{0}:{1}:{2},{3} Len={4}",
                time.Hour, time.Minute, time.Second, time.Millisecond, len);
            Console.WriteLine(rawPacket.ToString());
        }

        public static void sniffCapture(System.Net.IPAddress ipAddress, LibPcapLiveDevice device)
        {

            device.OnPacketArrival += device_OnPacketArrival;

            device.Open();

            device.StartCapture();

            // Wait for 'Enter' from the user.
            Console.ReadLine();

            device.StopCapture();

            device.Close();
        }

        public static LibPcapLiveDevice chooseDevice()
        {

            // Retrieve the device list
            var devices = LibPcapLiveDeviceList.Instance;

            // If no devices were found print an error
            if (devices.Count < 1)
            {
                Console.WriteLine("No devices were found on this machine");
                return null;
            }

            Console.WriteLine("The following devices are available on this machine:");
            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine();

            int i = 0;

            // Print out the available devices
            foreach (var dev in devices)
            {
                Console.WriteLine("{0}) {1} {2}", i, dev.Name, dev.Description);
                i++;
            }

            Console.WriteLine();
            Console.Write("-- Please choose a device for sending the ARP request: ");
            i = int.Parse(Console.ReadLine());

            var device = devices[i];
            return device;
        }

        public static System.Net.IPAddress getValidIPAddr()
        {
            System.Net.IPAddress ip;

            // loop until a valid ip address is parsed
            while (true)
            {
                Console.Write("-- Please enter IP address to be resolved by ARP: ");
                if (System.Net.IPAddress.TryParse(Console.ReadLine(), out ip))
                    break;
                Console.WriteLine("Bad IP address format, please try again");
            }

            return ip;

        }

        /*private static TargetList targetList = new TargetList();
        private static Attack attack = new Attack();

        static void Main(string[] args)
        {

            var device = Network_Utils.chooseDevice();
            System.Net.IPAddress ip = Network_Utils.getValidIPAddr();


            //Network_Utils.ARP_Request(ip, device);
            //Network_Utils.getArpResponse(ip, device);

            targetList.addNewTarget(device);
            attack.startAttack(device, targetList);
            


            /* Create a new ARP resolver
            ARP arper = new ARP(device);

            // print the resolved address or indicate that none was found
            var resolvedMacAddress = arper.Resolve(ip);
            if (resolvedMacAddress == null)
            {
                Console.WriteLine("Timeout, no mac address found for ip of " + ip);
            }
            else
            {
                Console.WriteLine(ip + " is at: " + arper.Resolve(ip));
            }
        }
        */
    }
}
