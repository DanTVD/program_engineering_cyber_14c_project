﻿using SharpPcap;
using SharpPcap.LibPcap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PacketDotNet;
using System.Net.NetworkInformation;
using System.Reflection;

namespace CSharp_ARP_Poisoning
{
    internal class CSharp_ARP_Poisoning
    {
        
        //--Variables
        private static Menu menu = new Menu();
        private static TargetList targetList = new TargetList();
        private static Attack attack = new Attack();

        static void Main(string[] args)
        {
            Console.Title = "v." + Assembly.GetExecutingAssembly().GetName().Version;

            while (true)
            {
                menu.printFrontend(targetList, attack);
                Console.Write("#>");
                string input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        menu.configureNetworkAdapter();
                        break;
                    case "2":
                        targetList.addNewTarget(menu.captureDevice);
                        break;
                    case "3":
                        targetList.printTargetList();
                        break;
                    case "4":
                        attack.startAttack(menu.captureDevice, targetList);
                        break;
                    case "5":
                        attack.forceStop();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
