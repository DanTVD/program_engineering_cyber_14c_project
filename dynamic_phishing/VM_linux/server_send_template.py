import socket
import time

import network
from network import send_msg, recv_msg

LISTEN = {'START_PORT': 4004, }

running = True


def client_msg_handler(sock):
    network.send_msg(sock, "Server: Linux")
    print(network.recv_msg(sock))
    raise Exception("Communication Finished")


def connect():
    # Create a TCP/IP socket
    listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Binding to local port
    server_address = ('', LISTEN['START_PORT'])
    listening_sock.bind(server_address)

    # Listen for incoming connections
    listening_sock.listen(1)

    # Create a new conversation socket
    client_soc, client_address = listening_sock.accept()

    connected = True
    # send_msg(client_soc, CONFIRM_MSG)

    global running
    while running:

        if not connected and running:
            # Listen for incoming connections
            listening_sock.listen(1)

            # Create a new conversation socket
            client_soc, client_address = listening_sock.accept()

            connected = True

            # send_msg(client_soc, CONFIRM_MSG)

        # Receiving data from the client
        # client_msg = recv_msg(client_soc)
        # print(client_msg)

        while connected:
            try:
                client_msg_handler(client_soc)  # menu(client_msg, connected)
            except Exception as e:
                connected = False
                time.sleep(5)

        # send_msg(client_soc, msg)

        # if client_msg == globals.OPTIONS[-1]:  # Exit
        # connected = False

        # Closing the conversation socket
        client_soc.close()
        connected = False

    # Closing the listening socket
    listening_sock.close()


def main():
    connect()


if __name__ == '__main__':
    main()
