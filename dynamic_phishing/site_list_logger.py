import socket
import requests
import scapy.all as scapy
from scapy.all import DNS, DNSQR, IP, sr1, srp1, UDP
import json
from selenium import webdriver
import re

import displaydns_parser

JSON_SITES = "sites.json"
JSON_LOG = "sites_log.txt"

GECKO_DRIVER = r"..\selenium_browser_drivers\geckodriver-v0.30.0-win64"
CHROME_DRIVER = r"..\selenium_browser_drivers\chromedriver_win32"


def json_nslookup(sites):
    for i in sites:
        # print(i, sites[i])
        # if len(sites[i]) == 0:
        sites[i] = socket.gethostbyname_ex(i)[-1]  # nslookup
        print(socket.gethostbyname_ex(i)[-1])

    return sites


def json_dns(sites):
    for i in sites:
        print()

        resp = requests.get(f'http://www.{i}/', stream=True)  # http req
        if not resp.ok:
            continue

        try:
            print(resp.raw._connection.sock.getpeername()[0], end='\n\n')
            sites[i].append(resp.raw._connection.sock.getpeername()[0])
        except AttributeError as e:
            pass

        dns_req = IP(dst='8.8.8.8') / UDP(dport=53) / DNS(rd=1, qd=DNSQR(qname=i))  # dns req / query
        answer = sr1(dns_req, timeout=2, verbose=0)

        if answer is not None:
            for ans in range(answer[DNS].ancount):
                if answer[DNS]['DNS Resource Record'][ans].type == 1:
                    sites[i].append(answer[DNS]['DNS Resource Record'][ans].rdata)
                    print(answer[DNS]['DNS Resource Record'][ans].rdata)
                    print(answer[DNS]['DNS Resource Record'][ans].rdata == resp.raw._connection.sock.getpeername()[0])
                elif answer[DNS]['DNS Resource Record'][ans].type == 28:
                    sites[i].append(answer[DNS]['DNS Resource Record'][ans].rdata)
                    print(answer[DNS]['DNS Resource Record'][ans].rdata)
                    print(answer[DNS]['DNS Resource Record'][ans].rdata == resp.raw._connection.sock.getpeername()[0])

        print()

        sites[i] = list(set(sites[i]))

    return sites


def json_dns_table(sites):
    driver = webdriver.Firefox(GECKO_DRIVER)
    for i in sites:
        driver.get('http://www.' + i)
        #driver.

        is_in_site = f"\w*\.*{i}"

        '''if i in dns_table:
            sites[i] += displaydns_parser.get_site_ip_from_dns_table(i)
            print(displaydns_parser.get_site_ip_from_dns_table(i))

        if 'www.' + i in dns_table:
            sites[i] += displaydns_parser.get_site_ip_from_dns_table('www.' + i)
            print(displaydns_parser.get_site_ip_from_dns_table('www.' + i))'''

        #print(is_in_site)
        dns_table = displaydns_parser.get_parsed_dns_table()
        for key in dns_table.keys():
            if re.search(is_in_site, key):
                sites[i] += displaydns_parser.get_site_ip_from_dns_table(key, dns_table)
                print(key, displaydns_parser.get_site_ip_from_dns_table(key, dns_table))
            #print(re.search(is_in_site, key))

        print()

        sites[i] = list(set(sites[i]))

    driver.close()

    return sites


def json_init():
    with open(JSON_SITES, 'r') as f:
        sites = json.loads(f.read())

    sites = json_dns_table(json_dns(json_nslookup(sites)))

    with open(JSON_SITES, 'w') as f:
        f.write(json.dumps(sites))

    return sites


sites = json_init()


def is_addr_in_sites(ip, sites: dict):
    # print(socket.gethostbyaddr(ip)[0])

    for i, j in zip(sites.keys(), sites.values()):
        # print(ip, j)
        for k in j:
            if ip == k:
                # print(ip, j)
                return i

    return -1


def log_sites(src_ip, site_ip):
    global sites

    site = is_addr_in_sites(site_ip, sites)

    if site != -1:
        with open(JSON_LOG, 'a') as f:
            f.write(f"{src_ip} -> {site}\n")


if __name__ == "__main__":
    pass
