import glob

IMG_LOC = r".\IMGs"
HEADER_SIZE = 10
CLIENT_BUFFER = 16

seed = '0111100110110010'


def send_msg(sock, msg):
    # Sending data back
    # print(msg, len(msg))

    msg_length = str(len(str(msg))).zfill(HEADER_SIZE)
    total_msg = msg_length + str(msg)

    print(total_msg)

    sock.sendall(total_msg.encode())


def recv_msg(sock):
    # Receiving data from the server
    msg_len = sock.recv(HEADER_SIZE).decode()  # len of msg up to HEADER_SIZE digit number
    print('LEN: {}'.format(msg_len))
    msg = sock.recv(int(msg_len)).decode()

    # print(full_msg)
    return msg  # full_msg


def get_list_of_imgs(path):
    return ('\n'.join(glob.glob(r'{}\*.*'.format(path)))).split("\n")


def send_photo(sock, photo_len, path):
    # Sending data back
    # print(msg, len(msg))

    send_msg(sock, photo_len)

    with open(path, 'rb') as f:
        photo = f.read()

    sock.sendall(photo)


def recv_photo(sock, path):
    photo_len = recv_msg(sock)
    server_photo = sock.recv(int(photo_len))

    with open(path, 'wb') as f:
        f.write(server_photo)
